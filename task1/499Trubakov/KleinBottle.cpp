#include <iostream>
#include <vector>
#include <Mesh.hpp>


float xCoord(float theta, float phi, float aa) {
    return (aa + cos(phi / 2) * sin(theta) - sin(phi / 2) * sin(2 * theta)) * cos(phi);
}

float yCoord(float theta, float phi, float aa) {
    return (aa + cos(phi / 2) * sin(theta) - sin(phi / 2) * sin(2 * theta)) * sin(phi);
}

float zCoord(float theta, float phi) {
    return sin(phi / 2) * sin(theta) + cos(phi / 2) * sin(2 * theta);
}

float dx_du(float theta, float phi) {
    return cos(phi) * (cos(theta) * cos(phi/2.) - 2 * cos(2 * theta) * sin(phi / 2.));
}

float dx_dv(float theta, float phi) {
    return 1. / 2 * sin(theta) * sin(phi / 2.) * cos(phi) - sin(theta) * cos(phi / 2.) * (cos(theta) * cos(phi) + sin(phi));
}

float dy_du(float theta, float phi) {
    return sin(phi) * (cos(theta) * cos(phi / 2.) - 2 * cos(2 * theta) * sin(phi / 2.));
}

float dy_dv(float theta, float phi) {
    return cos(phi) * (-sin(2 * theta) * sin(phi / 2.) + sin(theta) * cos(phi / 2.) + 1) - 0.5 * sin(phi) * (sin(theta) * sin(phi / 2.) + sin(2 * theta) * cos(phi / 2.));
}

float dz_du(float theta, float phi) {
    return 2 * cos(2 * theta) * cos(phi / 2.) + cos(theta) * sin(phi / 2.);
}

float dz_dv(float theta, float phi) {
    return 0.5 * sin(theta)*(cos(phi / 2.) - 2 * cos(theta) * sin(phi / 2.));
}

glm::vec3 makePoint(float theta, float phi, float aa) {
    return glm::vec3(xCoord(theta, phi, aa), yCoord(theta, phi, aa), zCoord(theta, phi));
}

glm::vec3 getNormalVector(int i, int j, int n_polygons) {
    float theta = -glm::pi<float>() + (float) 2 * glm::pi<float>() * i / n_polygons;
    float phi = (float) 2 * glm::pi<float>() * j / n_polygons;
    glm::vec3 du(dx_du(theta, phi), dy_du(theta, phi), dz_du(theta, phi));
    glm::vec3 dv(dx_dv(theta, phi), dy_dv(theta, phi), dz_dv(theta, phi));
    return glm::normalize(glm::cross(du, dv));
}


MeshPtr makeKleinBottle(float aa, int n_polygons) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for (int i = 0; i < n_polygons; i++) {
        float theta = -glm::pi<float>() + (float) 2 * glm::pi<float>() * i / n_polygons;
        float next_theta = -glm::pi<float>() + (float) 2 * glm::pi<float>() * (i + 1) / n_polygons;
        for (int j = 0; j < n_polygons; j++) {
            float phi = (float) 2 * glm::pi<float>() * j / n_polygons;
            float next_phi = (float) 2 * glm::pi<float>() * (j + 1) / n_polygons;
            vertices.push_back(makePoint(theta, phi, aa));
            vertices.push_back(makePoint(next_theta, phi, aa));
            vertices.push_back(makePoint(next_theta, next_phi, aa));
            normals.push_back(getNormalVector(i, j, n_polygons));
            normals.push_back(getNormalVector(i + 1, j, n_polygons));
            normals.push_back(getNormalVector(i + 1, j + 1, n_polygons));
            vertices.push_back(makePoint(theta, phi, aa));
            vertices.push_back(makePoint(theta, next_phi, aa));
            vertices.push_back(makePoint(next_theta, next_phi, aa));
            normals.push_back(getNormalVector(i, j, n_polygons));
            normals.push_back(getNormalVector(i, j + 1, n_polygons));
            normals.push_back(getNormalVector(i + 1, j + 1, n_polygons));
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount((GLint)vertices.size());

    return mesh;
}
