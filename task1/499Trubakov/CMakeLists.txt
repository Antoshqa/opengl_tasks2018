cmake_minimum_required(VERSION 3.10)
include_directories(common)

set(SRC_FILES
    Main.cpp
    common/Application.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
)

MAKE_TASK(499Trubakov 1 "${SRC_FILES}")
