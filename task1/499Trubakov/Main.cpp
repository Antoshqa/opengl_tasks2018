#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <iostream>

#include "KleinBottle.cpp"


class KleinBottleApp : public Application {
public:
    ShaderProgramPtr _shader;

    void makeScene() override
    {
        Application::makeScene();

        _kleinBottleMesh = makeKleinBottle(_aa, _n_polygons);
        _kleinBottleMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));
        _cameraMover = std::make_shared<OrbitCameraMover>();
        _shader = std::make_shared<ShaderProgram>("./499TrubakovData/shaderNormal.vert", "./499TrubakovData/shader.frag");
    }

    void update() override
    {
        double dt = glfwGetTime() - _oldTime;
        _oldTime = glfwGetTime();
        _cameraMover->update(_window, dt);
        _camera = _cameraMover->cameraInfo();
        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
            if (_n_polygons < 400) {
                _n_polygons = _n_polygons + 10;
            }
        } else if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
            if (_n_polygons > 10) {
                _n_polygons = _n_polygons - 10;
            }
        }
        _kleinBottleMesh = makeKleinBottle(_aa, _n_polygons);
        _kleinBottleMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

    }

    void draw() override
    {
        Application::draw();
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        _shader->use();
        _shader->setFloatUniform("time", (float)glfwGetTime());
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shader->setMat4Uniform("modelMatrix", _kleinBottleMesh->modelMatrix());
        _kleinBottleMesh->draw();
    }

protected:
    MeshPtr _kleinBottleMesh;
    unsigned int _n_polygons = 50;
    const float _aa = 6.0;
};

int main() {
    KleinBottleApp app;
    app.start();

    return 0;
}
